from django.db import models
from django.contrib.auth.models import User
import uuid
from pessoa.models import Pessoa
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator

ACCESS_PUBLIC = 0
ACCESS_PRIVATE = 1
ACCESS_LEVEL_CHOICES = [
    (ACCESS_PUBLIC, 'Public'),
    (ACCESS_PRIVATE, 'Private'),
]

def current_year():
    return datetime.date.today().year

def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)

# Create your models here.

class Veiculo(models.Model):
    TIPO_VEICULO = (
        ('carro', 'Carro'),
        ('moto', 'Moto')
    )
    access_level = models.IntegerField(choices=ACCESS_LEVEL_CHOICES, default=ACCESS_PRIVATE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    proprietario = models.ForeignKey(Pessoa, null=True, blank=False, on_delete=models.SET_NULL)
    tipo = models.CharField(max_length=5, choices=TIPO_VEICULO, null=False, blank=False)
    cor = models.CharField(max_length=50, null=False, blank=False)
    modelo = models.CharField(max_length=50, null=False, blank=False)
    ano = models.PositiveIntegerField(null=False, blank=False,
        default=current_year(), validators=[MinValueValidator(1960), max_value_current_year])
    id = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    
    def __str__(self):
        if self.tipo == 'carro':
            return self.cor
        else:
            return self.modelo
