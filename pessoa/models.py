from django.db import models
from django.contrib.auth.models import User
import uuid
from django.db.models.fields import AutoField
from phonenumber_field.modelfields import PhoneNumberField

ACCESS_PUBLIC = 0
ACCESS_PRIVATE = 1
ACCESS_LEVEL_CHOICES = [
    (ACCESS_PUBLIC, 'Public'),
    (ACCESS_PRIVATE, 'Private'),
]

# Create your models here.

class Pessoa(models.Model):
    access_level = models.IntegerField(choices=ACCESS_LEVEL_CHOICES, default=ACCESS_PRIVATE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    nome = models.CharField(max_length=200, blank=False, null=False)
    email = models.EmailField(max_length=500, blank=False, null=False)
    telefone = PhoneNumberField(null=False, blank=False, unique=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)

    def __str__(self):
        return self.nome
